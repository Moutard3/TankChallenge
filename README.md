# The Game

---

## Environnement

Moteur de jeu : GODOT
Langage : GDScript
Reseau : GODOT
Versionnement : Git
Langue : Français

---

## Versionnement

### Convention de nommage des branches

Une branche doit respecter la convention de nommage suivante :

``` ASCII
<numéro de ticket>/<titre de la user story>[/<PoC>]
```

- numéro de ticket : Numéro affecté au ticket à traiter (onglet "issues" de GitLab)
- titre de la user story : Le titre de la user story correspond au titre du ticket.
- PoC : Correspond à "Proof of Concept". Permet de spécifier si la branche correspond à un prototype non intégré à l'application.
Permet de s'assurer qu'une technologie est utilisable dans le cadre du projet.

### Convention de nommage des commits

Un commit doit respecter la convention de nommage suivante :

``` ASCII
<Résumé>

[<Description>]
#<numéro du ticket>
```

- Résumé : Un résumé de 50 caractères (norme Git) contenant un résumé succinct du commit.
- Description : Description détaillée du contenu du commit, si besoin.
- numéro du ticket : Numéro du ticket (ou de l'"issue" GitLab) associé au commit.
