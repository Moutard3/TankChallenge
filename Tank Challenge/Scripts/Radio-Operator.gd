extends Node

var _AirStrike_enabled = false
var _AirStrike_explosionTime = 2
var _AirStrike_reloadTime = 30
var _AirStrike_reloadProgress = 100

func _ready():
	$"Content/Up Screen/MinimapContainer/Minimap/Map/Tank/Camera2D".free()
	$"Content/Up Screen/Controls/Special Attacks/AirStrikeBtn".connect("pressed", self, "_toggle_AirStrike")

func _process(delta):
	if _AirStrike_reloadProgress < 100:
		_AirStrike_reloadProgress += (100/_AirStrike_reloadTime) * delta
		_AirStrike_reloadProgress = min(_AirStrike_reloadProgress, 100)
		$"Content/Up Screen/Controls/Special Attacks/AirStrikePB".value = _AirStrike_reloadProgress
	$"Content/Up Screen/Controls/Special Attacks/AirStrikeBtn".disabled = _AirStrike_reloadProgress < 100
	var mouse = get_viewport().get_mouse_position()
	$targetCursor.position = mouse

func _physics_process(delta):
	$"Content/Up Screen/MinimapContainer/Minimap/Map/Tank".position = global_data.getPlayerPosition()
	$"Content/Up Screen/MinimapContainer/Minimap/Map/Tank".rotation = global_data.getPlayerRotation()
	$"Content/Up Screen/MinimapContainer/Minimap/Map/Tank/TankBody/BodyTank".playing = global_data.playerIsMoving()

func _toggle_AirStrike():
	if _AirStrike_reloadProgress == 100:
		_AirStrike_enabled = !_AirStrike_enabled
		if _AirStrike_enabled:
			$"Content/Up Screen/Controls/Special Attacks/AirStrikeBtn".text = 'Annuler la frappe aérienne'
		else:
			$"Content/Up Screen/Controls/Special Attacks/AirStrikeBtn".text = 'Frappe aérienne'

func _on_input_event(viewport, event, shape_idx):
	if (_AirStrike_reloadProgress == 100 && \
		_AirStrike_enabled && \
		event is InputEventMouseButton && \
		event.is_pressed() && \
		event.button_index == BUTTON_LEFT \
	):
		_toggle_AirStrike()
		_hide_target_cursor()
		_AirStrike_reloadProgress = 0
		var tmp_pos = $"Content/Up Screen/MinimapContainer/Minimap/Map".get_local_mouse_position()
		yield(get_tree().create_timer(_AirStrike_explosionTime), "timeout")
		global_data.rpc("AirStrike", tmp_pos)

func _hide_target_cursor():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$targetCursor.visible = false

func _show_target_cursor():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	$targetCursor.visible = true

func _on_mouse_exited():
	_hide_target_cursor()

func _on_mouse_entered():
	if (_AirStrike_reloadProgress == 100 && _AirStrike_enabled):
		_show_target_cursor()
