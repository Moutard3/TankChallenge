extends DamageableItem
class_name Tank

var _bodySprite : AnimatedSprite
var _bodyRotation : float = 0.0
export var _bodyRotationSpeed : float = 1.5
var _turretRotationSpeed : float = 0.018
const scn_explosion = preload("res://Scenes/ExplosionTank.tscn")
export var moveSpeed : float = 100
var _velocity : Vector2 = Vector2()

func _ready() -> void:
	self._bodySprite = get_node("TankBody/BodyTank")

func _updateAnimations(oldVelocity) -> void:
	self.setVelocity(self.getVelocity().normalized() * self.moveSpeed)
	if (self.getVelocity().length() > 0):
		if(oldVelocity.length() == 0 ):
			_setFrameTank()
		_setAnimationTank(true)
	elif(oldVelocity.length() > 0 ):
		_setAnimationTank(false)

func _updateRotation(delta) -> void:
	self.setRotation(self.getRotation() + (_bodyRotation * _bodyRotationSpeed * delta))

func _setAnimationTank(runAnimation : bool) -> void:
	if(runAnimation):
		self._bodySprite.play()
	else :
		self._bodySprite.stop()

func _setFrameTank() -> void:
	self._bodySprite.frame = (self._bodySprite.frame + 1) % 3

func makeTheTankMove(delta) -> void:
	self.setVelocity(self.move_and_slide(self.getVelocity()))
	setVelocity(self.getVelocity())
	self._updateRotation(delta)

func getPosition() -> Vector2:
	return self.position

func getVelocity() -> Vector2:
	return self._velocity

func setVelocity(newVelocity : Vector2) -> void:
	if(newVelocity.length() > self.moveSpeed):
		newVelocity = Vector2(newVelocity.x, newVelocity.y).clamped(self.moveSpeed)
	self._velocity = newVelocity

func getRotation() -> float:
	return $TankBody.rotation

func setRotation(newRotation : float) -> void:
	newRotation = fmod(newRotation, 2 * PI)
	$TankBody.rotation = newRotation

func explosion():
	var explosion = scn_explosion.instance()
	explosion.position = global_position
	explosion.scale = get_tree().get_root().find_node("Map", true, false).scale
	explosion.play()
	get_node('/root/Game').add_child(explosion)
