extends KinematicBody2D

var _armed = true
var _reloadTime = 2
var _reloadProgress = 100

func _ready():
	global_data.connect("shot_fired", self, "fire")

func _process(delta):
	if not _armed:
		_reloadProgress += (100/_reloadTime) * delta

func fire():
	if (_armed):
		if (has_node("/root/Game/Gunner")):
			_armed = false
			_reloadProgress = 0
			$ReloadTimer.start(_reloadTime)
		var shell = preload("res://Scenes/Shell.tscn").instance()
		shell.start($PositionShoot.global_position, $PositionShoot.global_rotation, get_node("../../").scale.x, PI/12)
		get_node("/root/Game").add_child(shell)
		return shell

func _on_ReloadTimer_timeout():
	_armed = true
	_reloadProgress = 100
	global_data.emit_signal("reloaded")
