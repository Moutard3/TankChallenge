extends RigidBody2D

var speed = 400.0
var lifeTime = 1.5
const scn_explosion = preload("res://Scenes/ExplosionShell.tscn")
var node_explosion
var damage : int = 10
var _scale = 1

func _on_timer_timeout():
	explosion()

func explosion():
	node_explosion.global_position = global_position
	get_node('/root/Game').add_child(node_explosion)
	queue_free()

func _on_Shell_body_entered(body):
	if !body.is_a_parent_of(self):
		explosion()

func start(shellPosition, shellRotation, shellScale, aimError):
	position = shellPosition
	rotation = shellRotation + rand_range(-aimError, aimError)
	scale = Vector2(shellScale, shellScale)
	speed = speed * shellScale
	
	apply_impulse(Vector2(), Vector2(0.0, speed).rotated(rotation))
	
	node_explosion = scn_explosion.instance()
	node_explosion.scale = Vector2(shellScale, shellScale)
	
	var timer = Timer.new()
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.wait_time = lifeTime
	timer.autostart = true
	add_child(timer)
	
