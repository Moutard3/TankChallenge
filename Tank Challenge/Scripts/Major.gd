extends Tank
class_name Major

const MAX_HEALTH = 100;

func _ready():
	global_data.connect("player_speed_changed", self, "updateSpeed")

func _process(delta):
	get_node("../CanvasLayer/GUI/objective").text = String(global_data.getEnnemiesCount())
	get_node("../CanvasLayer/GUI/speed").text = String(round(getVelocity().length() / 2))

func _physics_process(delta) -> void:
	self.makeTheTankMove(delta)

func _control() -> void:
	self._bodyRotation = 0
	if(self.moveSpeed == 0):
		pass
	else:
		if (Input.is_action_pressed("move_right")):
			self._bodyRotation += 1
		elif (Input.is_action_pressed("move_left")):
			self._bodyRotation -= 1
	
	var oldVelocity = self.getVelocity()
	self.setVelocity(Vector2())
	
	if (Input.is_action_pressed("move_down")):
		self.setVelocity(Vector2(-self.moveSpeed, 0).rotated(self.getRotation()))
	elif (Input.is_action_pressed("move_up")):
		self.setVelocity(Vector2(self.moveSpeed, 0).rotated(self.getRotation()))
	self._updateAnimations(oldVelocity)

func makeTheTankMove(delta) -> void:
	self._control()
	updateData(delta)

func updateSpeed() -> void:
	self.moveSpeed = global_data.getPlayerSpeed()

func updateData(delta) -> void:
	self.setVelocity(self.move_and_slide(self.getVelocity()))
	setVelocity(self.getVelocity())
	self._updateRotation(delta)
	$Turret.rotation = global_data.getPlayerTurretRotation()
	self.updateGlobalPlayerData()

func updateGlobalPlayerData() -> void:
	global_data.setPlayerPosition(self.getPosition())
	global_data.setPlayerRotation(self.getRotation())
	global_data.setPlayerIsMoving(self._bodySprite.is_playing())
