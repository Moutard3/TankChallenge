extends Tank

func _ready():
	global_data.connect("reloaded", self, "_on_reloaded")

func _physics_process(delta):
	_control()
	self.get_parent().position = (-global_data.getPlayerPosition() + (self.get_parent().get_parent().get_rect().size)) / 2
	self.position = global_data.getPlayerPosition()
	$TankBody.rotation = global_data.getPlayerRotation()
	$Turret.rotation = global_data.getPlayerTurretRotation()
	
	get_node("TankBody/BodyTank").playing = global_data.playerIsMoving()

func _process(delta):
	$"../../../../GridContainer/ReloadProgress".value = $Turret._reloadProgress

func _on_reloaded():
	get_node("../../../../GridContainer/FireButton").disabled = false

func _control() -> void:
	if ($Turret._armed && Input.is_action_just_pressed("ui_select")):
		self._on_FireButton_pressed()
	
	if (Input.is_action_pressed("move_right")):
		global_data.setPlayerTurretRotation(global_data.getPlayerTurretRotation() + self._turretRotationSpeed)
	elif (Input.is_action_pressed("move_left")):
		global_data.setPlayerTurretRotation(global_data.getPlayerTurretRotation() - self._turretRotationSpeed)

func _on_FireButton_pressed():
	global_data.rpc("player_fire")
	get_node("../../../../GridContainer/FireButton").disabled = true
