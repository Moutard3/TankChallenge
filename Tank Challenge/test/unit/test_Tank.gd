extends "res://addons/gut/test.gd"

var Tank = load("res://Scripts/Tank.gd")

var tank : Tank = null;

func before_each():
	tank = Tank.new();
	var tankBody = KinematicBody2D.new()
	tankBody.set_name('TankBody')
	tank.add_child(tankBody)

func test_spawn():
	assert_not_null(tank, "The tank must spawn. You know...")

func test_set_rotation():
	var epsilon = 0.001
	
	var rotation : float = 0
	tank.setRotation(rotation)
	assert_almost_eq(tank.getRotation(), rotation, epsilon)
	
	rotation = PI + 0.5
	tank.setRotation(rotation)
	assert_almost_eq(tank.getRotation(), rotation, epsilon)
	
	rotation = 2 * PI
	tank.setRotation(rotation)
	assert_almost_eq(tank.getRotation(), 0.0, epsilon,
	"2PI rad should be equal to 0 rad")
	
	rotation = 2 * PI + 1
	tank.setRotation(rotation)
	assert_almost_eq(tank.getRotation(), 1.0, epsilon,
	"2PI + 1 rad should be equal to 1 rad")
	
	tank.setRotation(4 * PI + 0.67)
	assert_almost_eq(tank.getRotation(), 0.67, epsilon,
	"4PI + 0.67 rad should be equal to 0.67 rad")

func test_set_velocity():
	var velocity : Vector2  = Vector2(100, 100)
	tank.setVelocity(velocity)
	assert_eq(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(100, 25)
	tank.setVelocity(velocity)
	assert_eq(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(90, 25)
	tank.setVelocity(velocity)
	assert_lt(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(100, 0)
	tank.setVelocity(velocity)
	assert_eq(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(39, 0)
	tank.setVelocity(velocity)
	assert_lt(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(0, 100)
	tank.setVelocity(velocity)
	assert_eq(tank.getVelocity().length(), tank.moveSpeed)
	
	velocity = Vector2(0, 39)
	tank.setVelocity(velocity)
	assert_lt(tank.getVelocity().length(), tank.moveSpeed)
