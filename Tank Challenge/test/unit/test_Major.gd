
extends "res://addons/gut/test.gd"

var majorClass = load('res://Scripts/Major.gd')
var major = null
var inputEvent = InputEventAction.new()
	
#known as Setup
func before_each():
	inputEvent.pressed = true
	major = double(majorClass).new()
	stub(major,'_physics_process').to_call_super()
	stub(major,'updateData').to_do_nothing()
	stub(major,'makeTheTankMove').to_call_super()
	stub(major,'_control').to_call_super()

#known as Teardown
func after_each():
	major = null
	inputEvent.pressed = false
	Input.parse_input_event(inputEvent)

func test_input_rotation_right():
	assert_eq(major._bodyRotation,0.0,"")
	inputEvent.action = "move_right"
	Input.parse_input_event(inputEvent)
	simulate(major,1,.1)
	assert_eq(major._bodyRotation,1.0,
	"test that right input rotate the player tank")
	
func test_input_rotation_left():
	assert_eq(major._bodyRotation,0.0,"")
	inputEvent.action = "move_left"
	Input.parse_input_event(inputEvent)
	simulate(major,10,.1)
	assert_eq(major._bodyRotation,-1.0,
	"test that left input rotate the player tank")
		
		
	
