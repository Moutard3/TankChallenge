extends "res://addons/gut/test.gd"

var Tank = load("res://Scripts/Tank.gd")

var tank = null;
var parent = null;

func before_each():
	parent = Node.new()
	tank = double(Tank).new();
	tank._health = 100
	tank._id = 0;
	stub(tank,'explosion').to_do_nothing()
	stub(tank,'_process').to_call_super()
	stub(tank,'setHealth').to_call_super()
	parent.add_child(tank)

func after_each():
	global_data.reset()
	
func test_stay_alive():
	tank._health = 1
	simulate(tank,1,.1)
	assert_call_count(tank, 'setHealth', 0,[0])
	assert_not_called(tank, 'explosion')
	assert_eq(tank._health, 1)
	
func test_death():
	tank._health = 0
	simulate(tank,1,.1)
	assert_call_count(tank, 'setHealth', 1,[0])
	assert_call_count(tank, 'explosion',1)
	assert_eq(tank._health, 0)
	
func test_death_negative_health():
	tank._health = -10
	simulate(tank,1,.1)
	assert_call_count(tank, 'setHealth', 1,[0])
	assert_call_count(tank, 'explosion',1)
	assert_eq(tank._health, 0)
	
func test_game_still_ennemy():
	global_data.addEnnemy()
	global_data.addEnnemy()
	global_data.setEnnemyHealth(0,0)
	global_data.setEnnemyHealth(1,50)
	tank.isEnnemy = true;
	tank._health = 0
	simulate(tank,1, .1)
	assert_call_count(tank, 'setHealth', 1, [0])
	assert_call_count(tank, 'explosion', 1)
	assert_eq(global_data.getEnnemiesCount(), 1)
	assert_not_called(tank,'endGame')
	
func test_game_end_no_ennemy():
	global_data.addEnnemy()
	global_data.setEnnemyHealth(0, 0)
	tank.isEnnemy = true;
	simulate(tank, 1, .1)
	assert_eq(global_data.getEnnemiesCount(), 0)
	assert_call_count(tank, 'endGame', 1, [true])
	
func test_game_end_player_die():
	tank.isMajor = true
	tank._health = 0
	simulate(tank,1,.1)
	assert_call_count(tank, 'endGame', 1, [false])
	
	
