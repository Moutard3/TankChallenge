extends Control

const _crown = preload("res://img/crown.png")
const test:bool = false

remote func start_game():
	gamestate.random_assignment()
	
	$HBoxContainer/VBoxContainer/Action.disabled = true
	get_node("HBoxContainer/VBoxContainer/Action").text += "(2)"
	yield(get_tree().create_timer(1.0), "timeout")
	get_node("HBoxContainer/VBoxContainer/Action").text[-2] = "1"
	yield(get_tree().create_timer(1.0), "timeout")
	
	if gamestate.players.size()==4 or test:
		var game = preload("res://Scenes/Game.tscn")
		var gameInstance = game.instance()
		get_tree().get_root().add_child(gameInstance)
		
		var roleNode
		if(gamestate.get_player_role() == "Major"):
			var mapNode = preload("res://Scenes/Map.tscn").instance()
			mapNode.name = "Map"
			get_node('/root/Game').add_child(mapNode)
			
			roleNode = preload("res://Scenes/Major.tscn").instance()
			roleNode.get_node("Tank").position = get_node("/root/Game/Map/Spawn").position
		else:
			roleNode = load("res://Scenes/"+gamestate.get_player_role().replace(" ", "-")+".tscn").instance()
		
		get_node("/root/Game").add_child(roleNode)
		get_node("/root/Lobby").queue_free()

sync func _log(what):
	$HBoxContainer/RichTextLabel.add_text(what + "\n")

func _on_Action_pressed():
	var players = gamestate.get_players();
	for id in players:
		rpc_id(id, "start_game")
	start_game()

func _on_ItemList_item_selected(index: int):
	if gamestate.isRoleAvailable(index):
		gamestate.set_player_role(get_tree().get_network_unique_id(), index, true)
