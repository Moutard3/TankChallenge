extends Node

# Default game port
const DEFAULT_PORT: int = 10567

# Max number of players
const MAX_PEERS: int = 3
const ALL_ROLES = ["Random","Major","Gunner","Radio Operator","Mechanic"]

# Name for my player
var player_name: String = "Good Name"
var player_role: int = 0

# Names for remote players in id:name format
var players: = {}
var playersRoles: = {}

# Signals to let lobby GUI know what's going on
signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)
signal name_already_taken()

# Callback from SceneTree
func _player_connected(id: int) -> void:
	pass


# Callback from SceneTree
func _player_disconnected(id: int) -> void:
	if get_tree().is_network_server():
		unregister_player(id)
		for p_id in players:
			# Erase in the server
			rpc_id(p_id, "unregister_player", id)


# Callback from SceneTree, only for clients (not server)
func _connected_ok() -> void:
	# Registration of a client beings here, tell everyone that we are here
	rpc("register_player", get_tree().get_network_unique_id(), player_name,0)
	emit_signal("connection_succeeded")


# Callback from SceneTree, only for clients (not server)
func _server_disconnected() -> void:
	emit_signal("game_error")
	#emit_signal("game_error", "Server disconnected")
	end_game()


# Callback from SceneTree, only for clients (not server)
func _connected_fail() -> void:
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")


# Lobby management functions
remote func register_player(id: int, new_player_name: String, role:int) -> void:
	if get_tree().is_network_server():
		if players.values().has(new_player_name):
			rpc_id(id, "error_same_name")
			return
		# If we are the server, let everyone know about the new player
		rpc_id(id, "register_player", 1, player_name, player_role) # Send myself to new dude
		for p_id in players: # Then, for each remote player
			rpc_id(id, "register_player", p_id, players[p_id], playersRoles[p_id]) # Send player to new dude
			rpc_id(p_id, "register_player", id, new_player_name,0) # Send new dude to player
	playersRoles[id] = role
	players[id] = new_player_name
	emit_signal("player_list_changed")
	
remote func error_same_name() -> void:
	emit_signal("name_already_taken")

remote func unregister_player(id: int) -> void:
	players.erase(id)
	playersRoles.erase(id)
	emit_signal("player_list_changed")

remote func post_start_game() -> void:
	get_tree().set_pause(false) # Unpause and unleash the game!

var players_ready: = []


remote func ready_to_start(id: int) -> void:
	assert(get_tree().is_network_server())

	if not id in players_ready:
		players_ready.append(id)

	if players_ready.size() == players.size():
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func get_players()-> Dictionary:
	return players
	
func host_game(new_player_name: String) -> void:
	player_name = new_player_name
	player_role = 0
	playersRoles[1] = 0
	players[1] = new_player_name
	createServer(get_tree(), DEFAULT_PORT)

# Created to be used in unit tests to allow RPC usage
func createServer(tree : SceneTree, portNumber: int = DEFAULT_PORT) -> void:
	var host = NetworkedMultiplayerENet.new()
	host.create_server(portNumber, MAX_PEERS)
	tree.network_peer = host

# Created to be used in tests to simulate clients connections and game both.
func createClient(tree : SceneTree,
				ipAddress : String, port : int = DEFAULT_PORT) -> void:
	var client = NetworkedMultiplayerENet.new()
	client.create_client(ipAddress, port)
	tree.set_network_peer(client)

func join_game(ip: String, new_player_name: String) -> void:
	player_name = new_player_name
	player_role = 0
	createClient(get_tree(), ip)

func get_player_list() -> Array:
	return players.values()

func get_player_roles() -> Array:
	return playersRoles.values()

func get_player_name() -> String:
	return player_name
	
remote func set_playerList_Roles(rolesByPlayer) -> void:
	playersRoles = rolesByPlayer
	emit_signal("player_list_changed")

remote func set_playerList(ListOfPlayers) -> void:
	players = ListOfPlayers

func get_player_role() -> String:
	return ALL_ROLES[player_role]

remote func set_player_role(id: int, roleID: int, propagate:bool) -> void:
	if get_tree().is_network_server():
		rpc_id(id, "set_player_role", id, roleID, false)
		playersRoles[id] = roleID
		for p_id in players:
			if (p_id == get_tree().get_network_unique_id()):
				continue
			#rpc_id(p_id, "set_player_role", id, roleID, false)
			rpc_id(p_id, "set_playerList", players)
			rpc_id(p_id, "set_playerList_Roles", playersRoles)
	else :
		if propagate:
			rpc_id(1, "set_player_role", id, roleID, false)
	playersRoles[id] = roleID
	if id == get_tree().get_network_unique_id():
		player_role = roleID
	emit_signal("player_list_changed")

func isRoleAvailable(roleID:int)->bool:
	if roleID==0:
		return true
	var playerRoles = gamestate.get_player_roles()
	return !playerRoles.has(roleID)

remote func end_game() -> void:
	emit_signal("game_ended")
	players.clear()
	playersRoles.clear()
	get_tree().set_network_peer(null) # End networking

func random_assignment() -> void:
	var playerRoles = gamestate.get_player_roles()
	var availableRoles = ["Major","Gunner","Radio Operator","Mechanic"]
	for p_num in range(0,players.size()):
		while playerRoles[p_num]==0:
			randomize()
			var random = randi()%4+1
			if isRoleAvailable(random):
				if p_num==0:
					set_player_role(1,random,false)
					playerRoles[p_num]=random
				else:
					var peers = get_tree().get_network_connected_peers()
					set_player_role(peers[p_num-1],random,false)
					playerRoles[p_num]=random

func _ready() -> void:
	# Called when the node is added to the scene for the first time.
	# Initialization here
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
