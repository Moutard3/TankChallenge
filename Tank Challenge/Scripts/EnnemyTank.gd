extends Tank
class_name EnnemyTank

var _turret : EnemyTurret

func _init():
	global_data.addEnnemy()
	self._id = global_data.getEnnemyLastId()
	self._health = 50

func _ready():
	self._turret = $TankBody/Turret
	global_data.rpc("setEnnemyHealth", self._id, _health)

func _physics_process(delta) -> void:
	control(delta)

func control(delta):
	if get_parent() is PathFollow2D:
		get_parent().set_offset(get_parent().get_offset() + self.moveSpeed  * delta)
		position = Vector2()
		_setAnimationTank(true)
	else:
		pass

func _on_Visibility_body_entered(body):
	if(body.is_in_group("Player") && !body.is_in_group("not_targetable")):
		_turret.target = body

func _on_Visibility_body_exited(body):
	if body == _turret.getTarget():
		_turret.target = null
