extends "res://addons/gut/test.gd"

var MapScene = load('res://Scenes/Map.tscn')
var TankScene = load('res://Scenes/Tank.tscn')
var Tank = load('res://Scripts/Tank.gd')
var EnnemiTank = load('res://Scripts/EnnemyTank.gd')
var mapScene = null
var tankScene = null
var tank = null
var ennemi = null
	
#known as Setup
# TODO : Correct redundancy between tank and tankScene
func before_each():
	mapScene = MapScene.instance()
	tankScene = TankScene.instance()
	tankScene.position = mapScene.get_node("Spawn").position
	mapScene.add_child(tankScene)
	tank = Tank.new()
	ennemi = EnnemiTank.new()
	ennemi.add_to_group("MobileObject")

#known as Teardown
func after_each():
	mapScene = null
	tankScene = null
	tank = null
	ennemi = null

func test_collision_method():
	tank.setVelocity(Vector2(100, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 95, "")

func test_collision_method_medium_speed():
	tank.setVelocity(Vector2(50, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 97, "")

func test_collision_method_null_speed():
	tank.setVelocity(Vector2(0, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 100, "")

func test_collision_method_ennemi_max_speed():
	tank.setVelocity(Vector2(0,0))
	ennemi.setVelocity(Vector2(100, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 95, "")

# Vitesse max

func test_collision_method_ennemi_medium_speed():
	tank.setVelocity(Vector2(0,0))
	ennemi.setVelocity(Vector2(50, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 97, "")
	
func test_collision_method_ennemi_null_speed():
	tank.setVelocity(Vector2(0,0))
	ennemi.setVelocity(Vector2(0, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 100, "")

func test_collision_method_both_same_speed():
	tank.setVelocity(Vector2(100,0))
	ennemi.setVelocity(Vector2(100, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 90, "")

func test_collision_method_different_speeds():
	tank.setVelocity(Vector2(50,0))
	ennemi.setVelocity(Vector2(100, 0))
	tank._on_collision_damage(ennemi)
	assert_eq(tank._health, 92, "")

func test_collision_with_wall():
	pass
	# Tester en keypress, avec un yield
	
	# print(tank.getPosition)
	# tank.setVelocity(Vector2(0, -100))
	# tank.move_and_collide(tank.getVelocity())
	# print(tank.getPosition())
	# assert_eq(tank._health, 95, "")
