extends Control

func _on_ExitBtn_pressed():
	get_tree().quit()

func _on_ToLobbyBtn_pressed():
	gamestate.end_game()
	
	var LobbyNode = load("res://Scenes/Lobby.tscn").instance()
	get_tree().get_root().add_child(LobbyNode)
	queue_free()
