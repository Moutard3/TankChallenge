extends Area2D

signal collision_damage

func _ready():
	self.connect("collision_damage", self.get_parent().get_parent(), "_on_collision_damage")

func _on_areaDetectionTankBody_body_entered(body):
	if(!body.is_in_group("Ennemy") && !body.is_in_group("no_collide") && get_tree().get_root().has_node('Game/Major')):
		emit_signal("collision_damage", body)
