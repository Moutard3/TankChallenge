extends KinematicBody2D
class_name EnemyTurret

export (float) var _reloadTime = 5.0

var _turretRotation : float = 0.0;

var target
var hit_pos
var can_shoot = true
var id

func _ready():
	id = get_parent().get_parent()._id
	global_data.rpc("setEnnemyTurretRotation", self.id, _turretRotation)

func _physics_process(delta):
	if target && target.name == "Tank" && !target.is_in_group("not_targetable"):
		aim()
		global_data.rpc_unreliable("setEnnemyTurretRotation", self.id, _turretRotation)
	self.global_rotation = global_data.getEnnemyTurretRotation(self.id)
	

func aim():
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray($PositionShoot.global_position, target.global_position, [get_node("../../")], collision_mask )
	if result:
		if result.collider.name == "Tank":
			self._turretRotation = (result.position - global_position).angle() 
			global_rotation = _turretRotation
			if can_shoot:
				shoot()

func shoot():
	var shell = preload("res://Scenes/Shell.tscn").instance()
	shell.start($PositionShoot.global_position, $PositionShoot.global_rotation, get_tree().get_root().find_node("Map", true, false).scale.x, PI/6)
	get_node("/root/Game").add_child(shell)
	can_shoot = false
	$GunTimer.start(_reloadTime)

func getTarget():
	return self.target

func _on_GunTimer_timeout():
	self.can_shoot = true
