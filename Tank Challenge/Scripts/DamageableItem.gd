extends KinematicBody2D
class_name DamageableItem

export var isMajor : bool = false
export var isPlayer : bool = false
export var isEnnemy : bool = false
var _health : int = 100
var _id: int

signal damage_taken

func _ready():
	isMajor = (self.get_parent().get_name() == "Major")
	isPlayer = (self.is_in_group("Player"))
	isEnnemy = (self.is_in_group("Ennemy"))
	connect('damage_taken', self, '_on_damage_taken')

func _process(delta):
	if (isPlayer):
		self._health = global_data.getPlayerHealth()
	elif (isEnnemy):
		self._health = global_data.getEnnemyHealth(self._id)
	
	if (self._health <= 0):
		setHealth(0)
		if (isMajor):
			endGame(false)
		elif (isEnnemy):
			var ennemiesLeft = global_data.removeEnnemy()
			if ennemiesLeft <= 0:
				endGame(true)
		explosion()
		self.queue_free()
		
func endGame(playerWin):
	get_node("/root/Game").rpc("endGame", playerWin)

func setHealth(value: int) -> void:
	self._health = value
	if (isMajor):
		global_data.setPlayerHealth(self._health)
	elif (self.is_in_group("Ennemy")):
		global_data.rpc("setEnnemyHealth", self._id, self._health)

func _on_damage_taken(amount):
	setHealth(self._health - amount)

# Fonction implémentée par les enfants pour définir l'explosion adéquate
func explosion():
	pass

func _on_collision_damage(bodyCollider):
	var damageTaken = 0
	if (bodyCollider.is_in_group("Ammo")):
		damageTaken = bodyCollider.damage
		bodyCollider.damage = 0
	elif (bodyCollider.is_in_group("MobileObject")):
		damageTaken = (self._velocity.length() + bodyCollider.getVelocity().length()) * 0.05
	else:
		damageTaken = (self._velocity.length()) * 0.05
	damageTaken = round(damageTaken)
	self._on_damage_taken(damageTaken)
