extends Node

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if (Input.is_action_pressed("ui_cancel")):
		get_tree().quit()

remotesync func endGame(victory: bool):
	var scene
	if (victory):
		scene = preload("res://Scenes/Victory.tscn").instance()
	else:
		scene = preload("res://Scenes/Defeat.tscn").instance()
	get_tree().get_root().add_child(scene)
	queue_free()
